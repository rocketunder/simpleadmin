<?php
/* @var $this AgencyController */
/* @var $data Agency */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agencyname')); ?>:</b>
	<?php echo CHtml::encode($data->agencyname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactname')); ?>:</b>
	<?php echo CHtml::encode($data->contactname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />


</div>